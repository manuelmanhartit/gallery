<?php

class GalleryRenderer {
	private $ada;
	private $galleryReader;
	private $config;
	private $stringHelper;
	private $fileUri;

	function __construct($ada) {
		$this->ada = $ada;
		$this->config = $ada->getSiteConfigValue("gallery");
		$this->config['galleryUri'] = $ada->getSiteConfigValue("gallery.galleryUri");
		$this->config['imageUri'] = $ada->getSiteConfigValue("gallery,imageUri");
		$this->config['rootPath'] = $ada->getSiteConfigValue("gallery.rootPath");

		$this->galleryReader = new GalleryReader($this->config);
		$this->stringHelper = new StringHelper();
		$this->fileUri = $this->ada->getFileNameFromRequestParam($this->ada->DEFAULT_FILE_PARAM);
	}

	function print() {
		if ($this->_getImageUri()) {
			$this->ada->printResource($this->galleryReader->getInternalPath($this->fileUri), 'image/jpeg');
			return true;
		} else if ($this->_getGalleryUri()) {
			$this->printContent($this->fileUri);
			return true;
		} else {
			echo "no image uri and no gallery uri found - config: " . implode(", ", $this->config);
			return false;
		}
	}

	function _getImageUri() {
		return $this->_getUri($this->ada->getSiteConfigValue('gallery.imageUri'));
	}

	function _getGalleryUri() {
		return $this->_getUri($this->ada->getSiteConfigValue('gallery.galleryUri'));
	}

	function _getUri($name) {
		if ($this->fileUri && $this->stringHelper->startsWith($this->fileUri, $name)) {
			return $name;
		}
		return false;
	}

	function parseGalleryPath($fileUri) {
		// TODO move to ids instead of paths for security reasons
		if ($galleryUri = $this->_getGalleryUri()) {
			if (strpos($fileUri, '..') === false) {
				return substr($fileUri, strlen($galleryUri));
			}
		}
		// Root gallery
		return '/';
	}

	function printContent($fileUri) {
		// print the header		
		$this->ada->printPartialFromConfig('templates.before');
		if (! $this->config) {
			echo "<h2>Error</h2><p>No config could be loaded.</p>";
		} else {
			// fetch all the data
			$gallery = $this->galleryReader->readGallery($this->parseGalleryPath($fileUri));
			// print the gallery
			echo '<h2>' . 'Showing Gallery: ' . $gallery->name . '</h2><div>';
			//var_dump($gallery);
			if ($gallery !== false) {
				$this->printItems($gallery->galleries, true);
				$this->printItems($gallery->images);
			}
			echo '<div class="clear"></div></div>';
		}
		// print the footer
		$this->ada->printPartialFromConfig('templates.after');
	}

	function printItems($items, $isGallery = false) {
		echo '<div>';
		$count = 0;
		if ($isGallery) {
			$class='far fa-images';
		} else {
			$class='far fa-image';
		}
		foreach ($items as $item) {
			echo <<<EOT
<div class="gallery-container">
	<a class="gallery" href="$item->uri">
		<span class="image" style="background-image: url(&quot;$item->previewImage&quot;);"></span>
		<span class="text">
			<i class="$class"></i>
			$item->name
		</span>
	</a>
</div>
EOT;
		// Line Break
			if($count % 4 == 0) {
				echo <<<EOT
<!--
<div class="clear"></div>
-->
EOT;
			}
			$count++;
		}
		echo '</div>';
	}
/*
	function printImages($items) {
		echo '<div>';
		$count = 0;
		foreach ($items as $item) {
			echo <<<EOT
<!-- Image -->


<div class="thumb-wrapper col-md-3 col-sm-6 col-xs-12 thumb-14">
<div class="thumb-box squared">
<div class="thumb-loader">
<i class="glyphicon glyphicon-refresh gi-img-loader gi-spin"></i>
<span class="sr-only">Loading...</span>
</div>
<a href="$item->uri">
<div class="img-box">
<div class="square-bg" style="background-image: url(&quot;$item->previewImage&quot;);"></div>
</a></div></div>
		
<!--
<div class="square-bg" style="background-image: url(&quot;lib/php/image.php?max=512&amp;img=galleries/nature/david-moum-238279.jpg&quot;);"><img src="lib/php/image.php?max=512&amp;img=galleries/nature/david-moum-238279.jpg" data-src="lib/php/image.php?max=512&amp;img=galleries/nature/david-moum-238279.jpg" alt="david-moum-238279" class="thumb-img img-responsive img-thumb512 lazyload img-hidden"> </div>     <p class="image-caption dark-box">david-moum-238279 <span class="debug">(jpg / 512:, 256:, 128:)</span></p></div>
<div class="gallery-container">
	<div class="gallery">
		<a href="$item->uri">
			<img src="$item->previewImage" alt="" title=""/>
		</a>
	</div>
</div>
-->
EOT;
		// Line Break
			if($count % 4 == 0) {
				echo <<<EOT
<!--
<div class="clear"></div>
-->
EOT;
			}
			$count++;
		}
		echo '</div>';
	}
*/
}

?>
