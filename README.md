# Automatic Gallery 1.0

This project is for automatically creating Galleries from a folder on the hard drive.

In this README file you will find everything needed to run and adapt it.

## Getting Started

This project contains an `example-index.php` file, which can handle all http(s) calls. The script is rather short since it dispatches all requests to the gallery based on ada framework which do everything actually.

Actually this is more or less a plugin for your ada based project to easily integrate a gallery function.

## Prerequisites

* Installed Sass Compiler
* Any modern browser
* PHP development environment (if using docker eg. https://github.com/mikechernev/dockerised-php.git)

## Installing / Running

Clone this repository into an ADA based project.

Add following section into your `site-config.json`:

	"gallery": {
		"galleryUri": "/g",
		"rootPath": "/pictures",
		"infoFile": ".gallery-info.json",
		"imageExtensions": ["png", "jpg", "jpeg", "gif"],
		"imageUri": "/images"
	},

Have a look at the `example-index.php`, copy it into your project directory and adapt if neccessary.

## Built With

* ADA
* Scss Compiler
* PHP
* jQuery
* Bootstrap 4

## Contributing

Contact the author.

## Versioning

This project uses a release date as versioning. You can find it in the `site-config.json` file namely `lastUpdate`.

## Authors

* **Manuel Manhart** - *Initial work*

## Special Thanks


* **Yogesh Singh** - For [How to make Photo Gallery from image Directory with PHP](https://makitweb.com/make-photo-gallery-from-image-directory-with-php/)

## License

This project is licensed under MIT License - see the [LICENSE](https://opensource.org/licenses/MIT) for details.

## Open issues

* Create an edit mask to create gallery info files and let the user choose a name and preview image
* Create support for generating preview images in background
