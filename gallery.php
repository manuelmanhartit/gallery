<?php

class Gallery {
	public $name;
	public $path;
	public $uri;
	public $isNew;
	public $previewImage;
	public $galleries;
	public $images;

	function __construct() {
		$this->galleries = array();
		$this->images = array();
	}
}

class Image {
	public $name;
	public $description;
	public $path;
	public $uri;
	public $previewImage;
}

?>
