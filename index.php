<?php
// include the Ada framework
require_once('ada/framework.php');
// include the Gallery
require_once('gallery-renderer.php');
// include the Gallery Objects
require_once('gallery.php');
// include the Gallery Reader
require_once('gallery-reader.php');

// let the gallery handle the page
$g = new GalleryRenderer(new Ada());
$g->print();

?>
