<?php

// include the Gallery Objects
require_once('gallery.php');

/**
 * Be aware that we rely on fileHelper and stringHelper as given dependencies.
 */
class GalleryReader {
	private $config;
	private $rootGalleryUri;
	private $rootGalleryPath;
	private $imageUri;
	private $fileHelper;
	private $stringHelper;

	function __construct($config) {
		$this->config = $config;
		$this->fileHelper = new FileHelper();
		$this->stringHelper = new StringHelper();
		$this->rootGalleryUri = $this->config['galleryUri'];
		$this->imageUri = $this->config['imageUri'];
		$this->rootGalleryPath = $this->config['rootPath'];
	}

	/**
	 * reads one Gallery with a preview of all Sub-Galleries and Images
	 */
	function readGallery($uri) {
		// Image extensions
		$galleries = array();
		$images = array();
		
		if ($uri && strpos($uri, '..') === false) {
			$dir = $this->getInternalPath($uri);
			if (!$this->stringHelper->startsWith($dir, $this->rootGalleryPath)) {
				$dir = $this->fileHelper->concatPath($this->rootGalleryPath, $dir);
			}
		} else {
			$dir = $this->rootGalleryPath;
		}

		if (is_dir($dir) && ($dh = opendir($dir))) {
			$gallery = $this->_readGallery($dir, '');

			while (($file = readdir($dh)) !== false) {
				if ($file != '' && $file != '.' && $file != '..') {
					if ($o = $this->_readGallery($dir, $file)) {
						array_push($galleries, $o);
					} else if ($o = $this->_readImage($dir, $file)) {
						array_push($images, $o);
					}
				}
			}
			closedir($dh);

			usort($galleries, "sortByName");
			$gallery->galleries = $galleries;
			usort($images, "sortByName");
			$gallery->images = $images;
			return $gallery;
		}
	}

	function _loadGalleryInfo($dir) {
		return $this->fileHelper->loadJsonConfig($this->fileHelper->concatPath($dir, $this->config['infoFile']));
	}

	/**
	 * Reads one Gallery - only the gallery information itself without any images or subgalleries
	 */
	function _readGallery($parentDir, $item) {
		// internal path
		$absoluteFile = $this->fileHelper->concatPath($parentDir, $item);
		if (is_dir($absoluteFile)) {
			$galleryInfo = $this->_loadGalleryInfo($absoluteFile);
			$g = new Gallery();
			$externalUri = $this->getExternalUri($absoluteFile);
			if ($galleryInfo === false) {
				$g->isNew = true;
				$g->name = $item;
				if ($item === '') {
					$g->name = substr($parentDir, strrpos($parentDir, '/') + 1);
				}
				// TODO load first image from dir
				$g->previewImage = $this->_findPreviewImage($absoluteFile);
			} else {
				$g->name = $galleryInfo['name'];
				$g->previewImage = $this->fileHelper->concatPath($externalUri, $galleryInfo['previewImage']);
			}
			$g->path = $absoluteFile;
			$g->uri = $this->fileHelper->concatPath($this->rootGalleryUri, $externalUri);
			return $g;
		}
		return false;
	}

	function _findPreviewImage($absoluteFile) {
		if (is_dir($absoluteFile) && ($dh = opendir($absoluteFile))) {
			while (($file = readdir($dh)) !== false) {
				if ($file != '' && $file != '.' && $file != '..') {
					if ($o = $this->_readImage($absoluteFile, $file)) {
						closedir($dh);
						return $o->previewImage;
					}
				}
			}
			closedir($dh);
		}
	}

	function _readImage($parentDir, $item) {
		// internal path
		$absoluteFile = $this->fileHelper->concatPath($parentDir, $item);
		$thumbnailUri = $this->getExternalUri($absoluteFile);
		$imgUri = $this->getExternalUri($absoluteFile);
		$thumbnailExt = pathinfo($thumbnailUri, PATHINFO_EXTENSION);
		$imageExt = pathinfo($imgUri, PATHINFO_EXTENSION);
		$supportedExtensions = $this->config['imageExtensions'];

		if (in_array($thumbnailExt, $supportedExtensions) && 
				in_array($imageExt, $supportedExtensions)) {
			$i = new Image();
			$i->path = $absoluteFile;
			$i->uri = $imgUri;
			$i->previewImage = $thumbnailUri;
			$i->name = $item;
			//var_dump($i);
			return $i;
		}
		return false;
	}

	function getInternalPath(...$externalUri) {
		$temp = $this->fileHelper->concatPath(...$externalUri);
		return $this->stringHelper->replaceFirst($this->imageUri, $this->rootGalleryPath, $temp);
	}

	function getExternalUri(...$internalPath) {
		$temp = $this->fileHelper->concatPath(...$internalPath);
		return $this->stringHelper->replaceFirst($this->rootGalleryPath, $this->imageUri, $temp);
	}
}

function sortByName($itemA, $itemB) {
	return strcmp($itemA->name, $itemB->name);
}

?>
